<?php
namespace app\controllers;

use core\App;
use core\SessionUtils;
use core\Utils;
use core\RoleUtils;
use core\ParamUtils;
//use core\Validator;
use app\forms\PersonRegisterForm;
use core\Validator;

class PersonRegisterCtrl{
    private $form;
    private $dbase;



    public function __construct()
    {
    $this->form=new PersonRegisterForm();
    $this->dbase=App::getDB();
    }

public function validateRegister()
{

    //Pobranie wartosci
    $this->form->login=ParamUtils::getFromRequest('login',true,'Brak loginu');
    $this->form->email=ParamUtils::getFromRequest('email',true,'Brak email');
    $this->form->password=ParamUtils::getFromRequest('password',true,'Brak hasła');
    $this->form->rPassword=ParamUtils::getFromRequest('rPassword',true,'Brak powtórzeonego hasla');
    $this->form->city=ParamUtils::getFromRequest('city',true,'Brak miasta');
    $this->form->postalCode=ParamUtils::getFromRequest('postalCode',true,'Brak kodu pocztowego');
    $this->form->street=ParamUtils::getFromRequest('street',true,'Brak ulicy');
    $this->form->homeNumber=ParamUtils::getFromRequest('homeNumber',true, 'brak numeru domu');
    $this->form->phoneNumber=ParamUtils::getFromRequest('phoneNumber',true, 'brak numeru telefonu');




//Sprawdzenie czy sa puste
    if(empty(trim($this->form->login)))//       LOGIN
    {
        Utils::addErrorMessage('Podaj Login','login');
    }
    if(empty(trim($this->form->password)))//    PASSWORD
    {
        Utils::addErrorMessage('Podaj Haslo','password' );
    }
    if(empty(trim($this->form->rPassword)))//    REPEATED PASSWORD
    {
        Utils::addErrorMessage('Powtórz hasło','rPassword');
    }
    if(empty(trim($this->form->city)))//        CITY
    {
        Utils::addErrorMessage('Podaj Miasto','city');
    }
    if(empty(trim($this->form->postalCode)))//  POSTAL CODE
    {
        Utils::addErrorMessage('Podaj kod pocztowy','postalCode');
    }
    if(empty(trim($this->form->street)))//      STREET
    {
        Utils::addErrorMessage('Podaj ulicę','street');
    }
    if(empty(trim($this->form->homeNumber)))//  HOME NUMBER
    {
        Utils::addErrorMessage('Podaj numer domu','homeNumber');
    }
    if(empty(trim($this->form->email)))//  HOME NUMBER
    {
        Utils::addErrorMessage('Podaj adres email','email');
    }
    if(empty(trim($this->form->phoneNumber)))//  HOME NUMBER
    {
        Utils::addErrorMessage('Podaj numer telefonu','phoneNumber');
    }

    if (App::getMessages()->isError())
        return false;
    else
        return true;

}

    /**
     * Checking that login or email are in base
     */

    public function action_editProfile()
    {
        $id=SessionUtils::load('user_id',true);
        $user=$this->dbase->select('users','*',[
          'user_id'=>$id
       ]);
        $this->form->login=$user[0]['login'];
        $this->form->email=$user[0]['email'];
        $this->form->phoneNumber=$user[0]['phone_number'];
        $this->form->city=$user[0]['city'];
        $this->form->postalCode=$user[0]['postal_code'];
        $this->form->street=$user[0]['street'];
        $this->form->homeNumber=$user[0]['house_number'];

        $this->showEditProfile();


    }


    public function action_editProfileSave()
    {
        $valid=new Validator();
        $id=SessionUtils::load('user_id',true);
        $user=$this->dbase->select('users','*',[
            'user_id'=>$id
        ]);

        $this->form->email=ParamUtils::getFromRequest('email',true,'Podaj adres email');
        $this->form->phoneNumber=ParamUtils::getFromRequest('phoneNumber');
        $valid->validate($this->form->phoneNumber,['numeric'=>true]);
        if(!$valid->isLastOK())
        {
            Utils::addErrorMessage("Podaj prawidłowy numer telefonu w formie numerycznej",'phoneNumber');
        }
        $this->form->city=ParamUtils::getFromRequest('city');
        $this->form->postalCode=ParamUtils::getFromRequest('postalCode');
        $this->form->street=ParamUtils::getFromRequest('street');
        $this->form->homeNumber=ParamUtils::getFromRequest('homeNumber');

        $valid->validate($this->form->email,[
            'email'=>true
        ]);
        if(!$valid->isLastOK())
            Utils::addErrorMessage('Wprowadzony text nie jest adresem email','email');


        if(App::getMessages()->isEmpty()) {
            $this->dbase->update('users',
                [
                    'email' => $this->form->email,
                    'phone_number' => $this->form->phoneNumber,
                    'city' => $this->form->city,
                    'postal_code' => $this->form->postalCode,
                    'street' => $this->form->street,
                    'house_number' => $this->form->homeNumber,
                    'edition_date'=> date('Y-m-d')
                ],
                [
                    'user_id' => $id
                ]);
            Utils::addInfoMessage('Pomyślnie wprowadzono zmiany', 'zapis');
        }
        $this->action_editProfile();
    }
    private function showEditProfile()
    {
        App::getSmarty()->assign('form',$this->form);
        App::getSmarty()->display('editProfile.tpl');
    }
public function checkBase()

{
    $dblogin=$this->dbase->select('users',['login'],[
    'login'=>$this->form->login]);
    if(sizeof($dblogin)>0)
    {
        Utils::addErrorMessage('Istnieje już użytkownik o takim loginie','emailExist');
    }

    $dbemail=$this->dbase->select('users',['email'],['email'=>$this->form->email]);

    if(sizeof($dblogin)>0)
    {
        Utils::addErrorMessage('Istnieje już użytkownik o takim adresie email','emailExist');
    }

    return !App::getMessages()->isError();  //jesli nie ma bledu zwroci 1 jesli jest zwroci 0
    
}

public function action_showEditPerson()
{
$id=SessionUtils::load('user_id',true);


}
public function action_registerPerson()
{
if(count(App::getConf()->roles)>0) {
    App::getRouter()->forwardTo(App::getConf()->action_url);
    exit;
}
if($this->validateRegister() && $this->checkBase())
{
    /*
    DODAWANIE DO BAZY
    */
    $this->dbase->insert('users',
        [
         "login"=>$this->form->login,
         "email"=>$this->form->email,
         "password"=>md5($this->form->password),
         'city'=>$this->form->city,
         'postal_code'=>$this->form->postalCode,
         'street'=>$this->form->street,
         'house_number'=>$this->form->homeNumber,
         'phone_number'=>$this->form->phoneNumber,
         'creation_date'=>date('Y-m-d'),
         'edition_date'=>date('Y-m-d'),
            'role'=>'user'
        ]);
    Utils::addInfoMessage("Rejestracja przebiegła pomyślnie. Możesz teraz się zalogować",'register');
   $this->generateViewSucces();
}
else
{
    $this->generateView();
}
}

public function action_register()
{
    if(count(App::getConf()->roles)>0) {
        App::getRouter()->forwardTo(App::getConf()->action_url);
        exit;
    }

$this->generateView();
}

public function generateViewSucces(){

    App::getSmarty()->display('succes.tpl');
}
public function generateView()
{

    if(count(App::getConf()->roles)>0)
        App::getRouter()->forwardTo(App::getConf()->action_url);
    App::getSmarty()->assign('form', $this->form);
    //App::getSmarty()->assign();

    App::getSmarty()->display("register.tpl");
}




}