<?php
/**
 * Created by PhpStorm.
 * User: Łukasz
 * Date: 10.09.2018
 * Time: 04:17
 */

namespace app\controllers;

use core\App;
use core\SessionUtils;
use core\Utils;
use core\RoleUtils;
use core\ParamUtils;
use app\forms\ApplicationForm;

class ApplicationCtrl
{


    private $form;
    private $dbase;


    public function __construct()
    {
        $this->form = new ApplicationForm();
        $this->dbase = App::getDB();
    }

    public function action_saveApplication()
    {
    $this->form->topic=ParamUtils::getFromRequest('topic',true,'Brak tematu','topic');
    $this->form->description=ParamUtils::getFromRequest('description',true,'Brak opisu','description');
    $this->form->type=ParamUtils::getFromRequest('type',true,'Wybierz typ zgłoszenia','type');

    if(App::getMessages()->isEmpty())
    {
        try {
            $case_id;
            $this->dbase->insert('applications', [
                'topic' => $this->form->topic,
                'type_of_application' => $this->form->type,
                'is_open' => true,
            ]);
            $case_id = $this->dbase->id();
            $this->dbase->insert('messages',
                [
                    'user_id' => SessionUtils::load('user_id', true),
                    'application_id' => $case_id,
                    'message' => $this->form->description
                ]);
        }
        catch(\PDOException $e)
        {

            Utils::addInfoMessage("Wystąpił problem z bazą danych, spróbuj ponownie później",'case');
            $this->generateSuccesView($case_id);
            exit();
        }
        Utils::addInfoMessage("Twoje zgłoszenie zostało przyjęte",'case');
        $this->generateSuccesView($case_id);
    }
    else
    {
    $this->generateView();
    }

    }

    /**
     * Pobiera wartosc z POST i zwraca wiadomosci
     *
     */
    public function action_getMessages()
    {
        $application_id=ParamUtils::getFromRequest('application_id');
        try {
            $mess = $this->dbase->select('messages',
                [
                    "[>]users" => ['user_id']
                ],
                '*',
                [
                    'application_id' => $application_id
                ]);

        }
        catch(\PDOException $e)
        {
            Utils::addInfoMessage("Wystąpił problem z bazą danych, spróbuj ponownie później",'case');
            echo "<h2> Wystąpił problem z bazą danych</h2>";
            exit();
        }
        App::getSmarty()->assign('messages',$mess);
        App::getSmarty()->assign('user_id',SessionUtils::load('user_id',true));

        echo App::getSmarty()->fetch('messages.tpl');


    }

    public function action_addMessage()
    {
        App::getDB()->insert('messages',
            [
                'user_id'=>SessionUtils::load('user_id',true),
                'application_id'=>ParamUtils::getFromRequest('application_id'),
                'message'=>trim(ParamUtils::getFromRequest('description'))
            ]);



        $id=SessionUtils::load('user_id',true);
        $base=$this->dbase->select('messages'
            ,
            [
                "[>]applications"=>["application_id"]
            ]

            ,'*',[

                'user_id'=>$id,"ORDER"=>["application_id"]

            ]
        );
        $arr=array();
        $last;

        foreach ($base as $b)
        {
            if(empty($arr))
            {
                $arr[] = $b;
                $last=$b['application_id'];
            }
            else
            {
                if($last!=$b['application_id']) {
                    $arr[] = $b;
                    $last=$b['application_id'];
                }
            }
        }

        $application_id=ParamUtils::getFromRequest('application_id');
        $mess=$this->dbase->select('messages',
            [
                "[>]users"=>['user_id']
            ],
            '*',
            [
                'application_id'=>$application_id
            ]);

        App::getSmarty()->assign('messages',$mess);
        App::getSmarty()->assign('user_id',SessionUtils::load('user_id',true));
        $messages= App::getSmarty()->fetch('messages.tpl');


        App::getSmarty()->assign('datas',$arr);
        App::getSmarty()->assign('messages',$messages);



        App::getSmarty()->display('Myapplications.tpl');

    }

    public function action_ShowMyApplications()
    {
        $id=SessionUtils::load('user_id',true);
        $base=$this->dbase->select('messages'
            ,
            [
            "[>]applications"=>["application_id"]
            ]

            ,'*',[

        'user_id'=>$id,"ORDER"=>["application_id"]

        ]
    );
        $arr=array();
        $last;

        foreach ($base as $b)
        {
            if(empty($arr))
            {
                $arr[] = $b;
                $last=$b['application_id'];
            }
            else
            {
                if($last!=$b['application_id']) {
                    $arr[] = $b;
                    $last=$b['application_id'];
                }
            }
        }

       App::getSmarty()->assign('datas',$arr);
        App::getSmarty()->display('Myapplications.tpl');
    }
    private function generateSuccesView($case_id)    {

        App::getSmarty()->assign('case_id',$case_id);
        App::getSmarty()->display('succes.tpl');

    }
    public function action_showApplication()
    {
        $this->generateView();
    }

/*
 $dblogin=$this->dbase->select('users',['login'],[
    'login'=>$this->form->login]);
 */
    private function getServices()
    {
        return $this->dbase->select('types_of_applications',['type_id','name','price','description']);
    }


    public function generateView()
    {
        App::getSmarty()->assign('services',$this->getServices());
        App::getSmarty()->display('application.tpl');


    }



}

/*




 */