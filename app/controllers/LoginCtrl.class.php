<?php
namespace app\controllers;

use core\App;
use core\SessionUtils;
use core\Utils;
use core\RoleUtils;
use core\ParamUtils;
use app\forms\LoginForm;
class LoginCtrl
{

    private $form;
    private $dbase;

    public function __construct()
    {
        $this->form = new LoginForm();
        $this->dbase = App::getDB();
    }

    public function validate()
    {
        $this->form->login = ParamUtils::getFromPost('login');
        $this->form->password = ParamUtils::getFromPost('password');
    



        if (!isset($this->form->login)) {
            return false;
        }

        if (empty($this->form->login)) {
            Utils::addErrorMessage('Nie podano loginu');
        }
        if (empty($this->form->password)) {
            Utils::addErrorMessage('Nie podano hasla');
        }
        if (App::getMessages()->isError()) {
            return false;
        }
        //sprawdzenie czy podany login oraz hasło są poprawne

        $dblogin = $this->dbase->select('users', ['user_id','login', 'password', 'role'], [
            'login' => $this->form->login
        ]);


        if (sizeof($dblogin) == 0)//sprawdzenie czy znaleziono uzytkownika o takim loginie
        {
            Utils::addErrorMessage("Nie znaleziono użytkownika o takiej nazwie");
            return !App::getMessages()->isError();
        } else
            {
            if ($dblogin[0]['password'] == md5($this->form->password))//jesli haslo sie zgadza przyporządkowuje role w zaleznosci od tej ktora znajduje sie w bazie danych
            {   RoleUtils::addRole($dblogin[0]['role']);//ustawienie roli
                RoleUtils::addLogin($this->form->login);
                SessionUtils::store('user_id',$dblogin[0]['user_id']);



                App::getDB()->update('users',
                    [
                    'last_login'=>date("Y-m-d")
                    ],
                    [
                       'user_id'=>SessionUtils::load('user_id',true)
                    ]);
            } else {
                Utils::addErrorMessage('Złe hasło');
                return !App::getMessages()->isError();
            }
            return !App::getMessages()->isError();
        }

    }

    public function action_mainPage()
    {
        $this->generateView();
    }
    public function action_login()
    {
        if($this->validate())
        {
            Utils::addErrorMessage('Pomyślnie zalogowano');
            App::getRouter()->redirectTo("mainPage");// DODAC GDZIE MA PRZEKIEROWAC!!!

        }
            $this->generateView();
    }
    public function action_loginShow()
    {
        $this->action_login();
    }
    public function action_logout()
    {
        session_destroy();

        App::getRouter()->redirectTo('mainpage');
    }

    public function generateView()
    {
        App::getSmarty()->assign('form', $this->form);
        App::getSmarty()->display("mainPage.tpl");

    }


}

