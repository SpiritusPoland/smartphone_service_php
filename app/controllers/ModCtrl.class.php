<?php
namespace app\controllers;

use core\App;
use core\SessionUtils;
use core\Utils;
use core\RoleUtils;
use core\ParamUtils;
use app\forms\LoginForm;

class ModCtrl
{

   private $dbase;

    public function __construct()
    {

        $this->dbase = App::getDB();
    }

   public function action_EditUsers()
   {
       $users=App::getDB()->select('users',['login'],
           [
               'role[!]'=>'admin'
           ]);


       App::getSmarty()->assign('users',$users);
       App::getSmarty()->display('modusers.tpl');


   }

   public function action_getUser()
   {
       $login=ParamUtils::getFromRequest('usr');

       $userData=App::getDB()->select('users','*',['login'=>$login]);

    if(isset($userData[0]))
       {
           App::getSmarty()->assign('user', $userData[0]);
           $page = App::getSmarty()->fetch('userEdit.tpl');
           echo $page;
       }
       else
           echo "";

   }
    public function action_modGetMessages()//pobranie wiadomosci z danej aplikacji i wyswietlenie ich
    {

        $application_id=ParamUtils::getFromRequest('application_id');
        $mess=$this->dbase->select('messages',
            [
                "[>]users"=>['user_id']
            ],
            '*',
            [
                'application_id'=>$application_id
            ]);



        App::getSmarty()->assign('messages',$mess);
        App::getSmarty()->assign('user_id',SessionUtils::load('user_id',true));
        echo App::getSmarty()->fetch('modmessages.tpl');


    }
    public function action_modaddMessage()
    {
        App::getDB()->insert('messages',
            [
                'user_id'=>SessionUtils::load('user_id',true),
                'application_id'=>ParamUtils::getFromRequest('application_id'),
                'message'=>trim(ParamUtils::getFromRequest('description'))
            ]);



        $id=SessionUtils::load('user_id',true);
        $base=$this->dbase->select('messages'
            ,
            [
                "[>]applications"=>["application_id"]
            ]

            ,'*',[

                'user_id'=>$id,"ORDER"=>["application_id"]

            ]
        );
        $arr=array();
        $last;

        foreach ($base as $b)
        {
            if(empty($arr))
            {
                $arr[] = $b;
                $last=$b['application_id'];
            }
            else
            {
                if($last!=$b['application_id']) {
                    $arr[] = $b;
                    $last=$b['application_id'];
                }
            }
        }

        $application_id=ParamUtils::getFromRequest('application_id');
        $mess=$this->dbase->select('messages',
            [
                "[>]users"=>['user_id']
            ],
            '*',
            [
                'application_id'=>$application_id
            ]);

        App::getSmarty()->assign('messages',$mess);
        App::getSmarty()->assign('user_id',SessionUtils::load('user_id',true));
        $messages= App::getSmarty()->fetch('modmessages.tpl');


        $applications=App::getDB()->select('applications',
            [
                '[>]types_of_applications'=>[
                    'type_of_application'=>'type_id'
                ]

            ],'*',[	"ORDER" => ["datetime"=>"DESC"]]);


        App::getSmarty()->assign('messages',$messages);
        App::getSmarty()->assign('applications', $applications);
        App::getSmarty()->display('allApplications.tpl');


    }


   public function action_getApplications()
   {
       $page=ParamUtils::getFromGet('page');
       if(!isset($page))
           $page=1;
       if(!is_numeric($page))
           $page=1;

    $applications=App::getDB()->select('applications',
        [
            '[>]types_of_applications'=>[
                'type_of_application'=>'type_id'
            ]

        ],'*',[	"ORDER" => ["datetime"=>"DESC"]]);

   echo "wielkosc tablicy:".sizeof($applications);
    $pages=(int)(sizeof($applications)/5+1);
    echo "<br> ilosc stron:".$pages;
    $applications=array_slice($applications,($page-1)*5,5);
    App::getSmarty()->assign('pages', $pages);
    App::getSmarty()->assign('applications', $applications);
    App::getSmarty()->display('allApplications.tpl');

   }

   public function action_deleteUser()
   {
       try {
           $id = ParamUtils::getFromRequest('user_id');
           App::getDB()->delete('users', ['user_id' => $id]);
       }
       catch (\PDOException $e)
       {
           echo "<h3> Wystąpił problem z bazą danych<h3>";
           exit();
       }
       echo "<h2>Użytkownik został usunięty</h2>";

   }
    public function action_getApplication()
    {

    }

    public function action_getMyApplications()
    {

    }

}