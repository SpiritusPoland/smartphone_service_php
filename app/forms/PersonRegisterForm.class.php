<?php
/**
 * Created by PhpStorm.
 * User: Łukasz
 * Date: 03.09.2018
 * Time: 22:14
 */

namespace app\forms;


class PersonRegisterForm
{
    public $login;
    public $email;
    public $password;
    public $rPassword;
    public $city;
    public $postalCode;
    public $street;
    public $homeNumber;
    public $phoneNumber;
}