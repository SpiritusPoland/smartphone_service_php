





<h2>Użytkownik: <span style="font-weight: bold" >{$user['login']}</span></h2>
<form id="user" style="margin: 3%; width: 300px">

    <div class="row">
        <label for="login">Login:</label>
    </div>
    <div class="row">
        <input class="form-control" name="login" type="text" value="{$user['login']}"  id="login">

        <input name="user_id" value="{$user['user_id']}" type="hidden" >
    </div>
    <div class="row">
        <label for="email">Email:</label>
    </div>
    <div class="row">
        <input class="form-control" name="email" type="text" value="{$user['email']}" id="email">
    </div>
    <div class="row">
    <label for="phone_number">Numer telefonu:</label>
    </div>
    <div class="row">
        <input class="form-control"  type="text" value="{$user['phone_number']}" id="phone_number">
    </div>
    <div class="row">
        <label for="data">Data dołączenia:</label>
    </div>
    <div class="row">
        <input class="form-control"  type="date" value="{$user['creation_date']}" disabled id="data">
    </div>
    <div class="row">
        <label for="last_date">Data ostatniej aktywności::</label>
    </div>
    <div class="row">
        <input class="form-control" type="date" value="{$user['last_login']}" disabled id="last_date">
    </div>
    <div class="row">
        <label for="adres" style="margin-left: 10%;"><b>Adres:</b></label>
    </div>
    <div>
        <div id="adres">
            <div class="row" id="adres">
                <label for="miasto">Miasto</label>
            </div>
            <div class="row"  id="miasto">
                <input name="city" class="form-control" type="text" value="{$user['city']}" placeholder="Miasto">
            </div>

            <div class="row">
                <label for="kod">Kod Pocztowy</label>
            </div>
            <div class="row" id="kod">
                <input name="postal_code" class="form-control" class="form-control" type="text" value="{$user['postal_code']}" placeholder="Kod Pocztowy">
            </div>

            <div class="row">
                <label for="ulica">Ulica:</label>
            </div>
            <div class="row" id="ulica">
                <input class="form-control" name="street" type="text" value="{$user['street']}" placeholder="Ulica">
            </div>

            <div class="row">
                <label for="numer_domu">Numer domu:</label>
            </div>
            <div class="row" id="numer_domu">
                <input class="form-control" name="house_number" type="text" value="{$user['house_number']}" placeholder="Numer Domu">
            </div>
            <div class="row" style="width: 80%; margin: 5px auto 0;">
                <input class="btn btn-primary" type="submit" value="Zapisz">
                <input type="button" onClick="deleteUser()" class="btn btn-danger" value="Usuń">
            </div>
        </div>

    </div>
</form>
