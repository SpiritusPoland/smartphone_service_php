{extends file="main.tpl"}
{block name=content}

   <script type="text/javascript">
        $(document).ready(function ($) {
            $("#userlist").submit(function () {
                ajaxPostForm('userlist','{$conf->action_root}getUser', 'edituser');
                return false;
            });
        });

    </script>
    <script>
        function deleteUser() {

            var r=confirm("Czy napewno chcesz usunąć tego użytkownika?")
            if(r==true)
            {
                ajaxPostForm('user','{$conf->action_root}deleteUser', 'edituser');
            }
        }

    </script>




    <div class="container" style="margin-top: 10px; margin-bottom: 50px">
    <div class="row" id="poleregister" >
        <div class="col-sm-6 left-page">
            <div style="margin-left: 10px">
                <form id="userlist" method="post" action="{$conf->action_root}getUser"">
                    <div clas="row">
                        <label for="browsers">Użytkownicy</label>
                    </div>
                    <div class="row">
                        <input list="users" name="usr">
                        <datalist id="users">
                            {foreach $users as $user}
                                <option value="{$user['login']}">
                            {/foreach}
                        </datalist>
                        <button id="send" type="submit" class="btn btn-primary">Wybierz</button>
                    </div>
                </form>
            </div>

        </div>
        <div class="col-sm-6 right-page" >
        <div id="edituser">

        </div>

        </div>

    </div>


    {/block}