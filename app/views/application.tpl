{extends file="main.tpl"}
{block name=content}

<div class="container" style="margin-top: 10px; margin-bottom: 50px">
    <div class="row" style="text-align: center">
        <div class="col-sm-12" >
            <h1>Zgłoszenie</h1>
        </div>
    </div>
    <div class="row" id="poleregister">
        <div class="col-sm-6"  >
            <form method="post" id="position" style="margin-left: 3%" action="{$conf->action_root}saveApplication">
                <div class="form-group column" style="">

                    <div class="row">
                        <div class="col-xs-8">
                            <label for="temat">Temat:</label>

                            <div class="input-group">
                                <input type="text" class="form-control pole"  name='topic' placeholder="Temat zgłoszenia" name="temat"">
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-8">
                            <label for="zgloszenie">Rodzaj zgłoszenia:</label>
                            <div class="input-group pole" id="zgloszenie" >
                                <select class="form-control pole" name="type" >
                                    {foreach $services as $service}
                                    <option value="{$service['type_id']}">{$service['name']}</option>
                                    {/foreach}
                                </select>
                                <br>
                            </div>
                        </div>
                    </div>


                    <div class="row" >

                        <label for="opis">Opis:</label>
                        <div class="input-group" id="opis">
                            <textarea class="form-control pole" rows="5" id="description" name="description"></textarea>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <br/>
                            <button type="submit" class="btn btn-primary">Wyślij</button>
                        </div>
                    </div>
                </div>



            </form>
        </div>
        <div class="col-sm-6">

        </div>

    </div>
</div>


{/block}