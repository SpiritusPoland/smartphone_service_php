<div class="messages">



    {foreach $messages as $message}
    {if $user_id==$message['user_id']}
    <div class="message-container" style="width: auto">

        {/if}
        {if $user_id!=$message['user_id']}
        <div class="message-container darker" style="width: auto">

            {/if}
            <p> {$message['message']}</p>
            {if $user_id==$message['user_id']}
                <span class="time-right">{$message['login']}    {$message['creation_date']} </span>
            {/if}
            {if $user_id!=$message['user_id']}
                <span class="time-left">{$message['login']}   {$message['creation_date']} </span>
            {/if}

        </div>

        {/foreach}
        <form id="formul" method="post" action="{$conf->action_root}addMessage">
            <div class="form-group column mess" style="margin-left: 10px">
                <div class="row">
                    <label for="mess">Nowa wiadomość:</label>
                    <div class="input-group" id="mess">
                        <textarea id="new-message" rows="3" class="form-control" name="description"></textarea>
                        <input type="hidden" name="application_id" value="{$messages[0]['application_id']}">
                    </div>

                </div>
            </div>
            <div class="row" style="margin-top: 10px; margin-left: 5px">

                <button id="send" type="submit" class="btn btn-primary">Wyślij</button>

            </div>
    </div>
</div>
<div>

</div>