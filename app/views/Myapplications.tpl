{extends file="main.tpl"}
{block name='content'}
    <script type="text/javascript">
    $(document).ready(function ($) {
        $(".table-row").click(function () {
            var va ='form'+ $(this).attr("data-form");
            ajaxPostForm(va,'{$conf->action_root}getMessages', 'messages');
        });
    });

</script>



    <div class="container" style="margin-top: 10px; margin-bottom: 50px">
        <div class="row" style="text-align: center">
            <div class="col-sm-6 left-page" ">


                <h2>Twoje zgłoszenia:</h2>
                <div class="app">
                    <table class='table  table-bordered table-condensed table-striped table-hover '>
                        <tr>
                            <th>ID</th>
                            <th>Temat</th>
                            <th>Opis</th>
                        </tr>
                        {assign var=val value=1}
                        {foreach $datas as $data}
                            <!--data-href="{$conf->action_root}application?application={$data['application_id']}"-->
                            <tr class="table-row" data-form="{$val}">
                                <form id="form{$val}">
                                    <input type='hidden' name="application_id" value="{$data['application_id']}">
                                </form>
                                <td>{$data['application_id']}</td>
                                <td>{$data['topic']}</td>
                                <td>{$data['message']}</td>
                            </tr>
                            {$val=$val+1}
                        {/foreach}

                    </table>
                </div>
            </div>


            <div class="col-sm-6 right-page">
                <div id="messages">
{if isset($messages)}
    {$messages}
{/if}
                </div>


            </div>
        </div>
    </div>
{/block}