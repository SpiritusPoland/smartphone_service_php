{extends file="main.tpl"}
{block name=content}

    <!-- Środek-->




    <div class="col-sm-12" id="karuzelaBody" style="background-color: dimgrey" align="center">
        <div id="demo" class="carousel slide" data-ride="carousel">

            <!-- Indicators -->
            <ul class="carousel-indicators">
                <li data-target="#demo" data-slide-to="0" class="active"></li>
                <li data-target="#demo" data-slide-to="1"></li>
                <li data-target="#demo" data-slide-to="2"></li>
            </ul>

            <!-- The slideshow -->
            <div class="carousel-inner karuzela">
                <div class="carousel-item active">
                    <img class="img" src="{$conf->app_url}/images/main1.png" alt="tel1" >
                    <div class="img txtimg"> Telefon przestał działać i nie możesz go uruchomić?</h3>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="img" src="{$conf->app_url}/images/main2.jpg" alt="tel2" >
                    <div class="img txtimg">
                        Pęknięta szybka?
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="img" src="{$conf->app_url}/images/main3.jpg" alt="tel3">
                    <div class="img txtimg">
                        Pomożemy ze wszystkim!
                    </div>

                </div>
            </div>

            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#demo" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a>

        </div>


    </div>

{/block}