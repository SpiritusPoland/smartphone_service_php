{extends file="main.tpl"}
{block name=content}
    <div class="container" style="margin-top: 10px; margin-bottom: 50px">
        <div class="row" style="text-align: center">
            <div class="col-sm-12">
                <h1>Zarejestruj się</h1>
            </div>
        </div>
        <div class="row" id="poleregister">
            <div class="col-sm-6">

                <form id="position" style="margin-left: 3%" method="post" action="{$conf->action_root}registerPerson"
                ">
                <div class="form-group column" style="">
                    <div class="row">
                        <div class="col-sm-6 ">
                            <label for="login">Login:</label>

                            <div class="input-group" id="login">
                                <input type="text" class="form-control pole" placeholder="Login" name="login">
                            </div>
                        </div>
                        <div class="col-sm-6 message">
                            <!-- errors-->
                            <div class="error">
                                {if $msgs->isMessage('login')}
                                    {$msgs->getMessage('login')->text}
                                {/if}
                                {if $msgs->isMessage('loginExist')}
                                    {$msgs->getMessage('loginExist')->text}
                                {/if}
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <label for="email">Email:</label>
                            <div class="input-group" id="email">
                                <input type="email" class="form-control pole" placeholder="Email" name="email">
                            </div>
                        </div>
                        <div class="col-sm-6 message">
                            <div class="error">
                                {if $msgs->isMessage('emailExists')}

                                    {$msgs->getMessage('emailExists')->text}

                                {/if}

                                {if $msgs->isMessage('email')}
                                {$msgs->getMessage('email')->text}
                                {/if}
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <label for="haslo">Hasło:</label>

                            <div class="input-group" id="haslo">
                                <input type="password" class="form-control pole" placeholder="Hasło" name="password">
                            </div>
                        </div>
                        <div class="col-sm-6 message">
                            <div class="error">
                                {if $msgs->isMessage('password')}
                                    {$msgs->getMessage('password')->text}
                                {/if}
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-sm-6">
                            <label for="haslo">Powtórz Hasło:</label>
                            <div class="input-group" id="haslo">
                                <input type="password" class="form-control pole" placeholder="Powtórz hasło"
                                       name="rPassword">
                            </div>
                        </div>
                        <div class="col-sm-6 message">
                            <div class="error">
                                {if $msgs->isMessage('rPassword')}
                                    {$msgs->getMessage('rPassword')->text}
                                {/if}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <label for="adres">Adres:</label>
                            <div class="input-group" id="adres">
                                <input type="text" class="form-control pole" placeholder="Miasto"
                                       name="city" id="poleGora">
                            </div>
                        </div>
                        <div class="col-sm-6 message">
                            <div class="error">
                                {if $msgs->isMessage('city')}
                                    {$msgs->getMessage('city')->text}
                                {/if}
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-6">


                            <input type="text" class="form-control" placeholder="Kod Pocztowy" name="postalCode">
                        </div>
                        <div class="col-sm-6 message">
                            <div class="error">
                                {if $msgs->isMessage('postalCode')}
                                    {$msgs->getMessage('postalCode')->text}
                                {/if}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">

                            <input type="text" class="form-control" placeholder="Ulica" name="street">
                        </div>
                        <div class="col-sm-6 message">
                            <div class="error">
                                {if $msgs->isMessage('street')}
                                    {$msgs->getMessage('street')->text}
                                {/if}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <input type="text" class="form-control" placeholder="Numer budynku" name="homeNumber"
                                   id="pole-dol">
                        </div>
                        <div class="col-sm-6 message">
                            <div class="error">
                                {if $msgs->isMessage('homeNumber')}
                                    {$msgs->getMessage('homeNumber')->text}
                                {/if}
                            </div>
                        </div>
                    </div>


                    <div class="row">

                        <div class="col-sm-6" id="tel">
                            <label for="tel">Telefon:</label>
                            <div class="input-group" id="tel">
                                <input type="text" class="form-control pole" placeholder="Nr telefonu"
                                       name="phoneNumber">

                            </div>
                        </div>
                        <div class="col-sm-6 message">
                            <div class="error">
                                {if $msgs->isMessage('phoneNumber')}
                                    {$msgs->getMessage('phoneNumber')->text}
                                {/if}
                            </div>
                        </div>
                    </div>


                    <div class="row" style="margin-top: 10px; margin-left: 5px">

                        <button type="submit" class="btn btn-primary">Zarejestruj się</button>

                    </div>

                </div>

                </form>
            </div>

            <div class="col-sm-6 d-none d-md-block hidden-sm-down">
                <img src="{$conf->app_url}/images/HelloPhone.png">
                <br><br>
            </div>
        </div>
    </div>
{/block}