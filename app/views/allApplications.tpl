{extends file="main.tpl"}
{block name='content'}
    <script type="text/javascript">
        $(document).ready(function ($) {
            $(".table-row").click(function () {
                var va ='form'+ $(this).attr("data-form");
                ajaxPostForm(va,'{$conf->action_root}modGetMessages', 'messages');
            });
        });

    </script>



    <div class="container" style="margin-top: 10px; margin-bottom: 50px">
        <div class="row" style="text-align: center">
            <div class="col-sm-6 left-page" ">


            <h2>Zgłoszenia:</h2>
            <div class="app">
                <table class='table  table-bordered table-condensed table-striped table-hover '>
                    <tr>
                        <th>ID</th>
                        <th>Temat</th>
                        <th>Typ Zgłoszenia</th>
                    </tr>
                    {assign var=val value=1}
                    {foreach $applications as $appl}
                        <!--data-href="{$conf->action_root}application?application={$appl['application_id']}"-->
                        <tr class="table-row" data-form="{$val}">
                            <form id="form{$val}">
                                <input type='hidden' name="application_id" value="{$appl['application_id']}">
                            </form>
                            <td>{$appl['application_id']}</td>
                            <td>{$appl['topic']}</td>
                            <td>{$appl['name']}</td>
                        </tr>
                        {$val=$val+1}
                    {/foreach}


                </table>
            </div>
            <div class="row">
                <div style="align-content: center">
                Strony:
                {for $i=1 to $pages}

                    <a href="{$conf->action_root}getApplications/?page={$i}">{$i} </a>
                {/for}

                </div>
            </div>

        </div>



        <div class="col-sm-6 right-page">
            <div id="messages">
                {if isset($messages)}
                    {$messages}
                {/if}
            </div>


        </div>
    </div>
    </div>
{/block}