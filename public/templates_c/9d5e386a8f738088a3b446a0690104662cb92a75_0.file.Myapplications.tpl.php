<?php
/* Smarty version 3.1.30, created on 2018-09-16 20:52:15
  from "C:\xampp\htdocs\projekt\app\views\Myapplications.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b9ea65f623120_75102314',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9d5e386a8f738088a3b446a0690104662cb92a75' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projekt\\app\\views\\Myapplications.tpl',
      1 => 1537123921,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:main.tpl' => 1,
  ),
),false)) {
function content_5b9ea65f623120_75102314 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13838525615b9ea65f61fdc8_10107771', 'content');
$_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block 'content'} */
class Block_13838525615b9ea65f61fdc8_10107771 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php echo '<script'; ?>
 type="text/javascript">
    $(document).ready(function ($) {
        $(".table-row").click(function () {
            var va ='form'+ $(this).attr("data-form");
            ajaxPostForm(va,'<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
getMessages', 'messages');
        });
    });

<?php echo '</script'; ?>
>



    <div class="container" style="margin-top: 10px; margin-bottom: 50px">
        <div class="row" style="text-align: center">
            <div class="col-sm-6 left-page" ">


                <h2>Twoje zgłoszenia:</h2>
                <div class="app">
                    <table class='table  table-bordered table-condensed table-striped table-hover '>
                        <tr>
                            <th>ID</th>
                            <th>Temat</th>
                            <th>Opis</th>
                        </tr>
                        <?php $_smarty_tpl->_assignInScope('val', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['datas']->value, 'data');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['data']->value) {
?>
                            <!--data-href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
application?application=<?php echo $_smarty_tpl->tpl_vars['data']->value['application_id'];?>
"-->
                            <tr class="table-row" data-form="<?php echo $_smarty_tpl->tpl_vars['val']->value;?>
">
                                <form id="form<?php echo $_smarty_tpl->tpl_vars['val']->value;?>
">
                                    <input type='hidden' name="application_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['application_id'];?>
">
                                </form>
                                <td><?php echo $_smarty_tpl->tpl_vars['data']->value['application_id'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['data']->value['topic'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['data']->value['message'];?>
</td>
                            </tr>
                            <?php $_smarty_tpl->_assignInScope('val', $_smarty_tpl->tpl_vars['val']->value+1);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


                    </table>
                </div>
            </div>


            <div class="col-sm-6 right-page">
                <div id="messages">
<?php if (isset($_smarty_tpl->tpl_vars['messages']->value)) {?>
    <?php echo $_smarty_tpl->tpl_vars['messages']->value;?>

<?php }?>
                </div>


            </div>
        </div>
    </div>
<?php
}
}
/* {/block 'content'} */
}
