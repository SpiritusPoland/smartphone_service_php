<?php
/* Smarty version 3.1.30, created on 2018-09-11 12:07:35
  from "C:\xampp\htdocs\projekt\app\views\register.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b9793e72145c0_99181531',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1f25ed449283cb570fdbaf2bc21711992fc80da1' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projekt\\app\\views\\register.tpl',
      1 => 1536660449,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:main.tpl' => 1,
  ),
),false)) {
function content_5b9793e72145c0_99181531 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11842231045b9793e720f7d1_98418005', 'content');
$_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block 'content'} */
class Block_11842231045b9793e720f7d1_98418005 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <div class="container" style="margin-top: 10px; margin-bottom: 50px">
        <div class="row" style="text-align: center">
            <div class="col-sm-12">
                <h1>Zarejestruj się</h1>
            </div>
        </div>
        <div class="row" id="poleregister">
            <div class="col-sm-6">

                <form id="position" style="margin-left: 3%" method="post" action="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
registerPerson"
                ">
                <div class="form-group column" style="">
                    <div class="row">
                        <div class="col-sm-6 ">
                            <label for="login">Login:</label>

                            <div class="input-group" id="login">
                                <input type="text" class="form-control pole" placeholder="Login" name="login">
                            </div>
                        </div>
                        <div class="col-sm-6 message">
                            <!-- errors-->
                            <div class="error">
                                <?php if ($_smarty_tpl->tpl_vars['msgs']->value->isMessage('login')) {?>
                                    <?php echo $_smarty_tpl->tpl_vars['msgs']->value->getMessage('login')->text;?>

                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['msgs']->value->isMessage('loginExist')) {?>
                                    <?php echo $_smarty_tpl->tpl_vars['msgs']->value->getMessage('loginExist')->text;?>

                                <?php }?>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <label for="email">Email:</label>
                            <div class="input-group" id="email">
                                <input type="email" class="form-control pole" placeholder="Email" name="email">
                            </div>
                        </div>
                        <div class="col-sm-6 message">
                            <div class="error">
                                <?php if ($_smarty_tpl->tpl_vars['msgs']->value->isMessage('emailExists')) {?>

                                    <?php echo $_smarty_tpl->tpl_vars['msgs']->value->getMessage('emailExists')->text;?>


                                <?php }?>

                                <?php if ($_smarty_tpl->tpl_vars['msgs']->value->isMessage('email')) {?>
                                <?php echo $_smarty_tpl->tpl_vars['msgs']->value->getMessage('email')->text;?>

                                <?php }?>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <label for="haslo">Hasło:</label>

                            <div class="input-group" id="haslo">
                                <input type="password" class="form-control pole" placeholder="Hasło" name="password">
                            </div>
                        </div>
                        <div class="col-sm-6 message">
                            <div class="error">
                                <?php if ($_smarty_tpl->tpl_vars['msgs']->value->isMessage('password')) {?>
                                    <?php echo $_smarty_tpl->tpl_vars['msgs']->value->getMessage('password')->text;?>

                                <?php }?>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-sm-6">
                            <label for="haslo">Powtórz Hasło:</label>
                            <div class="input-group" id="haslo">
                                <input type="password" class="form-control pole" placeholder="Powtórz hasło"
                                       name="rPassword">
                            </div>
                        </div>
                        <div class="col-sm-6 message">
                            <div class="error">
                                <?php if ($_smarty_tpl->tpl_vars['msgs']->value->isMessage('rPassword')) {?>
                                    <?php echo $_smarty_tpl->tpl_vars['msgs']->value->getMessage('rPassword')->text;?>

                                <?php }?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <label for="adres">Adres:</label>
                            <div class="input-group" id="adres">
                                <input type="text" class="form-control pole" placeholder="Miasto"
                                       name="city" id="poleGora">
                            </div>
                        </div>
                        <div class="col-sm-6 message">
                            <div class="error">
                                <?php if ($_smarty_tpl->tpl_vars['msgs']->value->isMessage('city')) {?>
                                    <?php echo $_smarty_tpl->tpl_vars['msgs']->value->getMessage('city')->text;?>

                                <?php }?>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-6">


                            <input type="text" class="form-control" placeholder="Kod Pocztowy" name="postalCode">
                        </div>
                        <div class="col-sm-6 message">
                            <div class="error">
                                <?php if ($_smarty_tpl->tpl_vars['msgs']->value->isMessage('postalCode')) {?>
                                    <?php echo $_smarty_tpl->tpl_vars['msgs']->value->getMessage('postalCode')->text;?>

                                <?php }?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">

                            <input type="text" class="form-control" placeholder="Ulica" name="street">
                        </div>
                        <div class="col-sm-6 message">
                            <div class="error">
                                <?php if ($_smarty_tpl->tpl_vars['msgs']->value->isMessage('street')) {?>
                                    <?php echo $_smarty_tpl->tpl_vars['msgs']->value->getMessage('street')->text;?>

                                <?php }?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <input type="text" class="form-control" placeholder="Numer budynku" name="homeNumber"
                                   id="pole-dol">
                        </div>
                        <div class="col-sm-6 message">
                            <div class="error">
                                <?php if ($_smarty_tpl->tpl_vars['msgs']->value->isMessage('homeNumber')) {?>
                                    <?php echo $_smarty_tpl->tpl_vars['msgs']->value->getMessage('homeNumber')->text;?>

                                <?php }?>
                            </div>
                        </div>
                    </div>


                    <div class="row">

                        <div class="col-sm-6" id="tel">
                            <label for="tel">Telefon:</label>
                            <div class="input-group" id="tel">
                                <input type="text" class="form-control pole" placeholder="Nr telefonu"
                                       name="phoneNumber">

                            </div>
                        </div>
                        <div class="col-sm-6 message">
                            <div class="error">
                                <?php if ($_smarty_tpl->tpl_vars['msgs']->value->isMessage('phoneNumber')) {?>
                                    <?php echo $_smarty_tpl->tpl_vars['msgs']->value->getMessage('phoneNumber')->text;?>

                                <?php }?>
                            </div>
                        </div>
                    </div>


                    <div class="row" style="margin-top: 10px; margin-left: 5px">

                        <button type="submit" class="btn btn-primary">Zarejestruj się</button>

                    </div>

                </div>

                </form>
            </div>

            <div class="col-sm-6 d-none d-md-block hidden-sm-down">
                <img src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/images/HelloPhone.png">
                <br><br>
            </div>
        </div>
    </div>
<?php
}
}
/* {/block 'content'} */
}
