<?php
/* Smarty version 3.1.30, created on 2018-09-11 02:47:02
  from "C:\xampp\htdocs\projekt\app\views\application.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b9710861beb44_96801331',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1e58214884debc58a71201c4b866e1f868b5218a' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projekt\\app\\views\\application.tpl',
      1 => 1536626819,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:main.tpl' => 1,
  ),
),false)) {
function content_5b9710861beb44_96801331 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4607769005b9710861bbbf6_69620046', 'content');
$_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block 'content'} */
class Block_4607769005b9710861bbbf6_69620046 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


<div class="container" style="margin-top: 10px; margin-bottom: 50px">
    <div class="row" style="text-align: center">
        <div class="col-sm-12" >
            <h1>Zgłoszenie</h1>
        </div>
    </div>
    <div class="row" id="poleregister">
        <div class="col-sm-6"  >
            <form method="post" id="position" style="margin-left: 3%" action="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
saveApplication">
                <div class="form-group column" style="">

                    <div class="row">
                        <div class="col-xs-8">
                            <label for="temat">Temat:</label>

                            <div class="input-group">
                                <input type="text" class="form-control pole"  name='topic' placeholder="Temat zgłoszenia" name="temat"">
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-8">
                            <label for="zgloszenie">Rodzaj zgłoszenia:</label>
                            <div class="input-group pole" id="zgloszenie" >
                                <select class="form-control pole" name="type" >
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['services']->value, 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['service']->value['type_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['service']->value['name'];?>
</option>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                                </select>
                                <br>
                            </div>
                        </div>
                    </div>


                    <div class="row" >

                        <label for="opis">Opis:</label>
                        <div class="input-group" id="opis">
                            <textarea class="form-control pole" rows="5" id="description" name="description"></textarea>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <br/>
                            <button type="submit" class="btn btn-primary">Wyślij</button>
                        </div>
                    </div>
                </div>



            </form>
        </div>
        <div class="col-sm-6">

        </div>

    </div>
</div>


<?php
}
}
/* {/block 'content'} */
}
