<?php
/* Smarty version 3.1.30, created on 2018-09-17 17:41:57
  from "C:\xampp\htdocs\projekt\app\views\templates\main.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b9fcb45a31ed9_74308613',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f2f83f0df776d3fb4488d16fe3b1be60df353f2b' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projekt\\app\\views\\templates\\main.tpl',
      1 => 1537198915,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b9fcb45a31ed9_74308613 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<!doctype html>
<html lang="pl">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Serwis</title>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
		  integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
			integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
			crossorigin="anonymous"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
			integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
			crossorigin="anonymous"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
			integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
			crossorigin="anonymous"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/js/functions.js"><?php echo '</script'; ?>
>

	<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/css/style.css"
	</head>
<body style="background-color: #d6d8d6">


<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
mainpage">Serwis Smartfonów</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
mainpage">Strona Główna<span class="sr-only">(current)</span></a>
            </li>
			<?php if (isset($_smarty_tpl->tpl_vars['conf']->value->roles['user'])) {?>
			<?php if ($_smarty_tpl->tpl_vars['conf']->value->roles['user'] == true) {?>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
showApplication">Zgłoś naprawę</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
editProfile">Twój profil</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
ShowMyApplications">Twoje zgłoszenia</a>
				</li>

				<?php }?>
			<?php }?>

            <li class="nav-item">
                <a class="nav-link" href="#">O Nas</a>
            </li>

        </ul>
        <?php if (!isset($_smarty_tpl->tpl_vars['conf']->value->login)) {?>
            <a class="form-inline" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
register">Nie masz konta? Zarejestruj się!</a>
            <form class="form-inline" action="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
login" method="post">
                <fieldset>
                    <input class="form-control mr-sm-2" type="text" name="login" placeholder="Login" aria-label="Login">
                    <input class="form-control mr-sm-2" type="password" name="password" placeholder="Hasło" aria-label="Hasło">
                    <input class="btn btn-outline-success my-2 my-sm-0" type="submit" value="Zaloguj się" >
                </fieldset>
            </form>
        <?php }?>
        <?php if (isset($_smarty_tpl->tpl_vars['conf']->value->login)) {?>
            Witaj <?php echo $_smarty_tpl->tpl_vars['conf']->value->login;?>

            <a class="btn btn-outline-success my-2 my-sm-0" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
logout" role="button">Wyloguj</a>
        <?php }?>
    </div>
</nav>

<?php if (isset($_smarty_tpl->tpl_vars['conf']->value->roles['admin'])) {?>
	<?php if ($_smarty_tpl->tpl_vars['conf']->value->roles['admin'] == true) {?>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark navbar-pills">
			<a class="navbar-brand" href="#">Zarządzanie</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent02"
					aria-controls="navbarSupportedContent02" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse " id="navbarSupportedContent02">
				<ul class="navbar-nav mr-auto  ">
					<li class="nav-item">
						<a class="nav-link" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
EditUsers">Użytkownicy<span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item active">
						<a class="nav-link" href="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
getApplications"><b>Zgłoszenia</b></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Twoje odpowiedzi</a>
					</li>
				</ul>
			</div>
		</nav>


	<?php }?>

<?php }?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16939593795b9fcb45a264d8_74656393', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6259772505b9fcb45a2d5a4_52522428', 'footer');
?>

<div class="container">
	<div class="row">
	</br>
		</br>
	</div>


</div>
<div class="footer">
	<div class="container">
		<br>
		<div class="row">
			<div class ="col-sm-4" style="background: #F00F32">
				as
			</div>
			<div class ="col-sm-4" style="background: #f0c040">
				sa
			</div>
			<div class ="col-sm-4" style="background: aqua">
				saa
			</div>
		</div>
		sdfs<br><br>
	</div>
</div>





</body>

</html><?php }
/* {block 'content'} */
class Block_16939593795b9fcb45a264d8_74656393 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_6259772505b9fcb45a2d5a4_52522428 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'footer'} */
}
