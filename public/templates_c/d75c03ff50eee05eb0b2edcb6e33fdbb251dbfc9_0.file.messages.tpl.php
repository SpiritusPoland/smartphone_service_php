<?php
/* Smarty version 3.1.30, created on 2018-09-18 00:09:07
  from "C:\xampp\htdocs\projekt\app\views\messages.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5ba02603e19843_63532318',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd75c03ff50eee05eb0b2edcb6e33fdbb251dbfc9' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projekt\\app\\views\\messages.tpl',
      1 => 1537222146,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ba02603e19843_63532318 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="messages">



    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['messages']->value, 'message');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['message']->value) {
?>
    <?php if ($_smarty_tpl->tpl_vars['user_id']->value == $_smarty_tpl->tpl_vars['message']->value['user_id']) {?>
    <div class="message-container" style="width: auto">

        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['user_id']->value != $_smarty_tpl->tpl_vars['message']->value['user_id']) {?>
        <div class="message-container darker" style="width: auto">

            <?php }?>
            <p> <?php echo $_smarty_tpl->tpl_vars['message']->value['message'];?>
</p>
            <?php if ($_smarty_tpl->tpl_vars['user_id']->value == $_smarty_tpl->tpl_vars['message']->value['user_id']) {?>
                <span class="time-right"><?php echo $_smarty_tpl->tpl_vars['message']->value['login'];?>
    <?php echo $_smarty_tpl->tpl_vars['message']->value['creation_date'];?>
 </span>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['user_id']->value != $_smarty_tpl->tpl_vars['message']->value['user_id']) {?>
                <span class="time-left"><?php echo $_smarty_tpl->tpl_vars['message']->value['login'];?>
   <?php echo $_smarty_tpl->tpl_vars['message']->value['creation_date'];?>
 </span>
            <?php }?>

        </div>

        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

        <form id="formul" method="post" action="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
addMessage">
            <div class="form-group column mess" style="margin-left: 10px">
                <div class="row">
                    <label for="mess">Nowa wiadomość:</label>
                    <div class="input-group" id="mess">
                        <textarea id="new-message" rows="3" class="form-control" name="description"></textarea>
                        <input type="hidden" name="application_id" value="<?php echo $_smarty_tpl->tpl_vars['messages']->value[0]['application_id'];?>
">
                    </div>

                </div>
            </div>
            <div class="row" style="margin-top: 10px; margin-left: 5px">

                <button id="send" type="submit" class="btn btn-primary">Wyślij</button>

            </div>
    </div>
</div>
<div>

</div><?php }
}
