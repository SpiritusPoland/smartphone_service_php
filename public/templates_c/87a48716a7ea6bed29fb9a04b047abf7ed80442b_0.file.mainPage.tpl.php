<?php
/* Smarty version 3.1.30, created on 2018-09-03 10:06:41
  from "C:\xampp\htdocs\php_09_bd\app\views\mainPage.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b8ceb910c92c3_20284872',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '87a48716a7ea6bed29fb9a04b047abf7ed80442b' => 
    array (
      0 => 'C:\\xampp\\htdocs\\php_09_bd\\app\\views\\mainPage.tpl',
      1 => 1535961326,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:main.tpl' => 1,
  ),
),false)) {
function content_5b8ceb910c92c3_20284872 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20688501895b8ceb910c7085_12762024', 'content');
$_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block 'content'} */
class Block_20688501895b8ceb910c7085_12762024 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Serwis Smartfonów</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Strona Główna<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Zgłoś naprawę</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">O Nas</a>
                </li>

            </ul>
            <a class="form-inline" href="#">Nie masz konta? Zarejestruj się!</a>
            <form class="form-inline" action="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
login" method="post">
                <fieldset>
                <input class="form-control mr-sm-2" type="text" name="login" placeholder="Login" aria-label="Login">
                <input class="form-control mr-sm-2" type="password" name="password" placeholder="Hasło" aria-label="Hasło">
                <input class="btn btn-outline-success my-2 my-sm-0" type="submit" value="Zaloguj się" >
                </fieldset>
            </form>
        </div>
    </nav>
    <!-- Środek-->




    <div class="col-sm-12" id="karuzelaBody" style="background-color: dimgrey" align="center">
        <div id="demo" class="carousel slide" data-ride="carousel">

            <!-- Indicators -->
            <ul class="carousel-indicators">
                <li data-target="#demo" data-slide-to="0" class="active"></li>
                <li data-target="#demo" data-slide-to="1"></li>
                <li data-target="#demo" data-slide-to="2"></li>
            </ul>

            <!-- The slideshow -->
            <div class="carousel-inner karuzela">
                <div class="carousel-item active">
                    <img class="img" src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/images/main1.png" alt="tel1" >
                    <div class="txtimg"> Telefon przestał działać i nie możesz go uruchomić?</h3>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="img" src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/images/main2.jpg" alt="tel2" >
                    <div class="txtimg">
                        Pęknięta szybka?
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="img" src="<?php echo $_smarty_tpl->tpl_vars['conf']->value->app_url;?>
/images/main3.jpg" alt="tel3">
                    <div class="txtimg">
                        Pomożemy ze wszystkim!
                    </div>

                </div>
            </div>

            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#demo" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a>

        </div>


    </div>

<?php
}
}
/* {/block 'content'} */
}
