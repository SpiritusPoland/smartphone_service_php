<?php
/* Smarty version 3.1.30, created on 2018-09-18 02:34:41
  from "C:\xampp\htdocs\projekt\app\views\modusers.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5ba048212f02e7_58032941',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3c9a59d2fd65c27f400b91bf07d778716a34a0c7' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projekt\\app\\views\\modusers.tpl',
      1 => 1537230871,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:main.tpl' => 1,
  ),
),false)) {
function content_5ba048212f02e7_58032941 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10618765765ba048212ec139_44208598', 'content');
$_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block 'content'} */
class Block_10618765765ba048212ec139_44208598 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


   <?php echo '<script'; ?>
 type="text/javascript">
        $(document).ready(function ($) {
            $("#userlist").submit(function () {
                ajaxPostForm('userlist','<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
getUser', 'edituser');
                return false;
            });
        });

    <?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
>
        function deleteUser() {

            var r=confirm("Czy napewno chcesz usunąć tego użytkownika?")
            if(r==true)
            {
                ajaxPostForm('user','<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
deleteUser', 'edituser');
            }
        }

    <?php echo '</script'; ?>
>




    <div class="container" style="margin-top: 10px; margin-bottom: 50px">
    <div class="row" id="poleregister" >
        <div class="col-sm-6 left-page">
            <div style="margin-left: 10px">
                <form id="userlist" method="post" action="<?php echo $_smarty_tpl->tpl_vars['conf']->value->action_root;?>
getUser"">
                    <div clas="row">
                        <label for="browsers">Użytkownicy</label>
                    </div>
                    <div class="row">
                        <input list="users" name="usr">
                        <datalist id="users">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['users']->value, 'user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['user']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['user']->value['login'];?>
">
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                        </datalist>
                        <button id="send" type="submit" class="btn btn-primary">Wybierz</button>
                    </div>
                </form>
            </div>

        </div>
        <div class="col-sm-6 right-page" >
        <div id="edituser">

        </div>

        </div>

    </div>


    <?php
}
}
/* {/block 'content'} */
}
