-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 12 Wrz 2018, 21:46
-- Wersja serwera: 10.1.34-MariaDB
-- Wersja PHP: 7.1.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `phone_service`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `applications`
--

CREATE TABLE `applications` (
  `application_id` int(11) NOT NULL,
  `topic` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `type_of_application` varchar(100) CHARACTER SET utf32 COLLATE utf32_polish_ci NOT NULL,
  `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_open` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `messages`
--

CREATE TABLE `messages` (
  `message_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  `message` text COLLATE utf8_polish_ci NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `types_of_applications`
--

CREATE TABLE `types_of_applications` (
  `type_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `price` float NOT NULL,
  `description` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `types_of_applications`
--

INSERT INTO `types_of_applications` (`type_id`, `name`, `price`, `description`) VALUES
(1, 'Diagnostyka', 25.5, 'Wykonamy całkowitą diagnostykę twojego telefonu.Sprawdzimy jego stan techniczny, wszystkie jego sensory oraz podzespoły'),
(2, 'Założenie szybki ochronnej', 15, 'Założymy szybkę na ekran twojego telefonu, dzięki czemu uchronisz go przed rysami oraz zmniejszysz prawdopodobieństwo pęknięcia ekranu w przypadku większego urazu mechanicznego'),
(3, 'Wymiana podzespołów', 250, 'Cena uzależniona jest od marki oraz modelu telefonu');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `login` varchar(32) COLLATE utf8_polish_ci NOT NULL,
  `password` text COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `city` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `postal_code` varchar(6) COLLATE utf8_polish_ci NOT NULL,
  `street` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `house_number` varchar(5) COLLATE utf8_polish_ci NOT NULL,
  `phone_number` varchar(15) COLLATE utf8_polish_ci NOT NULL,
  `role` varchar(25) COLLATE utf8_polish_ci NOT NULL,
  `creation_date` date NOT NULL,
  `edition_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`user_id`, `login`, `password`, `email`, `city`, `postal_code`, `street`, `house_number`, `phone_number`, `role`, `creation_date`, `edition_date`) VALUES
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@mail.com', 'Sosnowiec', '41-215', 'Nowackiego', '5', '+48517544521', 'admin', '2018-09-03', '2018-09-03'),
(3, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'asdas@sadasd.pl', 'sdfsdf', '54-452', 'sdfsdf', '5', '5455422548', 'user', '2018-09-10', '2018-09-10'),
(4, 'user2', 'ee11cbb19052e40b07aac0ca060c23ee', 'user@sadasd.sad', 'asdad', 'sdfsdf', 'sdfsdf', 'sdfsd', 'sdf4sdfsd', 'user', '2018-09-11', '2018-09-11'),
(5, 'user3', 'ee11cbb19052e40b07aac0ca060c23ee', 'asdasd@asfdasd.pl', 'asdjasdj', 'soidfo', 'iokhnioksdh', 'idfsd', '564188485', 'user', '2018-09-11', '2018-09-11'),
(6, 'user4', 'ee11cbb19052e40b07aac0ca060c23ee', 'asdas@sad.pl', 'sdfio', 'ijaLKs', 'josdxjf', '3', '854548', 'user', '2018-09-11', '2018-09-11');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `applications`
--
ALTER TABLE `applications`
  ADD PRIMARY KEY (`application_id`);

--
-- Indeksy dla tabeli `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`message_id`),
  ADD KEY `application_id` (`application_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indeksy dla tabeli `types_of_applications`
--
ALTER TABLE `types_of_applications`
  ADD PRIMARY KEY (`type_id`);

--
-- Indeksy dla tabeli `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `applications`
--
ALTER TABLE `applications`
  MODIFY `application_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `messages`
--
ALTER TABLE `messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `types_of_applications`
--
ALTER TABLE `types_of_applications`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`application_id`) REFERENCES `applications` (`application_id`),
  ADD CONSTRAINT `messages_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
