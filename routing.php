<?php

use core\App;
use core\Utils;

App::getRouter()->setDefaultRoute('mainpage'); // akcja/ścieżka domyślna
App::getRouter()->setLoginRoute('login'); // akcja/ścieżka na potrzeby logowania (przekierowanie, gdy nie ma dostępu)

Utils::addRoute('mainpage',             'LoginCtrl');
Utils::addRoute('login',                'LoginCtrl');
Utils::addRoute('logout',               'LoginCtrl');


Utils::addRoute('registerPerson',       'PersonRegisterCtrl');
Utils::addRoute('register',             'PersonRegisterCtrl');
Utils::addRoute('editProfile',          'PersonRegisterCtrl',['user']);
Utils::addRoute('editProfileSave',      'PersonRegisterCtrl',['user']);


Utils::addRoute('showApplication',      'ApplicationCtrl',['user']);
Utils::addRoute('saveApplication',      'ApplicationCtrl',['user']);
Utils::addRoute('ShowMyApplications',   'ApplicationCtrl',['user']);
Utils::addRoute('getMessages',          'ApplicationCtrl',['user']);
Utils::addRoute('addMessage',           'ApplicationCtrl',['user']);

Utils::addRoute('EditUsers',            'ModCtrl',['admin']);
Utils::addRoute('getUser',            'ModCtrl',['admin']);
Utils::addRoute('getApplications',            'ModCtrl',['admin']);
Utils::addRoute('deleteUser',            'ModCtrl',['admin']);
Utils::addRoute('modGetMessages',            'ModCtrl',['admin']);
Utils::addRoute('modaddMessage',            'ModCtrl',['admin']);

/*Utils::addRoute('personNew',     'PersonEditCtrl',	['user','admin']);
Utils::addRoute('personEdit',    'PersonEditCtrl',	['user','admin']);
Utils::addRoute('personSave',    'PersonEditCtrl',	['user','admin']);
Utils::addRoute('personDelete',  'PersonEditCtrl',	['admin']);*/